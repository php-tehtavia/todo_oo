<h3><?=$title ?></h3>

<form action="/todo/create">
<div class="col-12">
<?= Config\services::validation()->listErrors(); ?>
<div class="form-group">
<label>Title</label>
<input class="form-control" name="title" palaceholder="Enter title" maxlength="255">
</div>
<div class="form-group">
<label>Description</label>
<textarea class="form-control" name="description" palaceholder="Enter Description" ></textarea>
</div>
<button class="btn btn-primary"> save</button>
</div>

</form>