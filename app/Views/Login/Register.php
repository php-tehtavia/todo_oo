<h3><?= $title ?> </h3>

<form action="/Login/Registration">
<div class="col-12">
<?=\Config\Services::validation()->listErrors(); ?>

<div class="form-group">
<label>Username</label>
<input class="form-control" name="username" placeholder="Enter username"
maxlength="30">
</div>
<div class="form-group">
<label>Firstname</label>
<input class="form-control" name="firstname" placeholder="Enter firstname"
maxlength="100">
</div>

<div class="form-group">
<label>Lastname</label>
<input class="form-control" name="lastname" placeholder="Lastname"
maxlength="100">
</div>

<div class="form-group">
<label>password</label>
<input class="form-control" name="password" placeholder="Enter password"
maxlength="255">
</div>

<div class="form-group">
<label>password again</label>
<input class="form-control" name="confirmpassword" placeholder="Enter password again"
maxlength="255">
</div>

<button class="btn btn-primary"> Register</button>
</div>
</form>