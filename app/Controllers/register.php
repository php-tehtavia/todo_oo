<?php namespace App\Controllers;

const REGISTER_TITLE = 'Todo - Register';


class Register extends BaseController 
{
    
    public function index()
    {
       
        $data['title'] = 'Todo - register';
        echo view('templates/header');
        echo view('login/register', $data);
        echo view('templates/footer');
        

    }
    public function register(){
        $data['title']  = REGISTER_TITLE;
        echo view('templates/header');
        echo view('login/register', $data);
        echo view('templates/footer');
    }
   

}

?>