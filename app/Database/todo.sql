drop database if exists todo;
 create database todo;
  use todo;

  create table user (
      id int PRIMARY key AUTO_INCREMENT,
      username VARCHAR(30) not null UNIQUE,
      password VARCHAR(255) not null,
      firstname VARCHAR (100),
      lastname VARCHAR (100)
  )

  create table task (
      id int primary key auto_increment,
      title VARCHAR(255) not null,
      added timestamp default CURRENT_TIMESTAMP,
     description text,
      user_id int not null,
      index (user_id),
      FOREIGN key (user_id) REFERENCES user(id)
      on delete RESTRICT
  )